package com.tanhdev.havanaonline.repository;

import com.tanhdev.havanaonline.model.Attribute;
import com.tanhdev.havanaonline.model.ProductAttribute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductAttributeRepository extends JpaRepository<ProductAttribute, Long> {

    @Query(value = "select * from product_attribute where product_id = :id", nativeQuery = true)
    List<ProductAttribute> findAllByProduct_Id(@Param("id") Long id);
}

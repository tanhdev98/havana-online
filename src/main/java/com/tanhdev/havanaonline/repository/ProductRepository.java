package com.tanhdev.havanaonline.repository;

import com.tanhdev.havanaonline.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findAllByCategory_Id(Long id);

    @Query(value = "select * from product order by product_id desc limit 8", nativeQuery = true)
    List<Product> findTopById();

    @Query(value = "select * from product order by field(product_tag,'hot') desc limit 8", nativeQuery = true)
    List<Product> findAllByTag();

    @Query("select p from Product p where p.name like %?1%")
    Page<Product> findAll(String keyword, Pageable pageable);

}
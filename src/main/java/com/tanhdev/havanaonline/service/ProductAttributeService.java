package com.tanhdev.havanaonline.service;

import com.tanhdev.havanaonline.model.ProductAttribute;
import com.tanhdev.havanaonline.repository.ProductAttributeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductAttributeService {
    @Autowired
    ProductAttributeRepository productAttributeRepository;

    public List<ProductAttribute> getAllProductAttribute() {
        return productAttributeRepository.findAll();
    }

    public void addProductAttribute(ProductAttribute productAttribute) {
        productAttributeRepository.save(productAttribute);
    }

    public void removeProductAttributeById(Long id) {
        productAttributeRepository.deleteById(id);
    }

    public Optional<ProductAttribute> getProductAttributeById(Long id) {
        return productAttributeRepository.findById(id);
    }

    public List<ProductAttribute> findAllByProduct_Id(Long id) {
        return productAttributeRepository.findAllByProduct_Id(id);
    }
}

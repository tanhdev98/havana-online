package com.tanhdev.havanaonline.service;

import com.tanhdev.havanaonline.model.Attribute;
import com.tanhdev.havanaonline.model.Category;
import com.tanhdev.havanaonline.repository.AttributeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AttributeService {
    @Autowired
    AttributeRepository attributeRepository;

    public List<Attribute> getAllAttribute() {
        return attributeRepository.findAll();
    }

    public void addAttribute(Attribute attribute) {
        attributeRepository.save(attribute);
    }

    public void removeAttributeById(Long id) {
        attributeRepository.deleteById(id);
    }

    public Optional<Attribute> getAttributeById(Long id) {
        return attributeRepository.findById(id);
    }
}

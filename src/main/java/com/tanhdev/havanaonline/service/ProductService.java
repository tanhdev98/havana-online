package com.tanhdev.havanaonline.service;

import com.tanhdev.havanaonline.model.Product;
import com.tanhdev.havanaonline.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public Page<Product> getAllProduct(int pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber - 1, 12);
        return productRepository.findAll(pageable);
    }

    public Page<Product> getAllProduct(String keyword, int pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber - 1, 12);

        if (keyword != null) {
            return productRepository.findAll(keyword, pageable);
        }
        return productRepository.findAll(pageable);
    }

    public List<Product> getNewProduct() {
        return productRepository.findTopById();
    }

    public List<Product> getHotProduct() {
        return productRepository.findAllByTag();
    }

    public void addProduct(Product product) {
        productRepository.save(product);
    }

    public void removeProductById(Long id) {
        productRepository.deleteById(id);
    }

    public Optional<Product> getProductById(Long id) {
        return productRepository.findById(id);
    }

    public List<Product> getAllProductsByCategoryId(Long id) {
        return productRepository.findAllByCategory_Id(id);
    }


//    public List<Product> getAllProduct(String keyword) {
//        if (keyword != null) {
//            return productRepository.findAllByName(keyword);
//        }
//        return productRepository.findAll();
//    }
}

package com.tanhdev.havanaonline.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    private Long id;

    @Column(name = "product_name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id", referencedColumnName = "category_id")
    private Category category;

    @Column(name = "product_price")
    private double price;

    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<ProductAttribute> listProductAttributes;

    @Column(name = "product_tag")
    private String tag;

    @Column(name = "product_image")
    private String imageName;

    @Column(name = "product_description")
    private String description;

    @OneToMany(mappedBy = "product")
    private List<Order> order;
}

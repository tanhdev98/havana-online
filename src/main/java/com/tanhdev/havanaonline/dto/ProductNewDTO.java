package com.tanhdev.havanaonline.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductNewDTO {
    private Long id;
    private String name;
    private double price;
    private String imageName;
    private Long quantity;
    private String productAttribute;
}

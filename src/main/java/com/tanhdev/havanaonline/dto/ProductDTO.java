package com.tanhdev.havanaonline.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO {
    private Long id;
    private String name;
    private Long categoryId;
    private double price;
    private String tag;
    private String imageName;
    private String description;
}

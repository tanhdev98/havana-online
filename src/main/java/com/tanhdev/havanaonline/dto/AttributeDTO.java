package com.tanhdev.havanaonline.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AttributeDTO {
    private Long id;
    private String size;
    private Long quantity;
}

package com.tanhdev.havanaonline.controller;

import com.tanhdev.havanaonline.global.GlobalData;
import com.tanhdev.havanaonline.model.Product;
import com.tanhdev.havanaonline.model.ProductAttribute;
import com.tanhdev.havanaonline.model.User;
import com.tanhdev.havanaonline.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class HomeController {
    @Autowired
    CustomUserDetailService customUserDetailService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    ProductService productService;
    @Autowired
    ProductAttributeService productAttributeService;
    @Autowired
    AttributeService attributeService;


    @GetMapping("/")
    public String homePage(Model model) {
        model.addAttribute("productsNew", productService.getNewProduct());
        model.addAttribute("productsHot", productService.getHotProduct());
        model.addAttribute("cartCount", GlobalData.productNewDTOList.size());
        model.addAttribute("categories", categoryService.getAllCategory());
        return "index";
    }

    @GetMapping("/about")
    public String getAbout(Model model) {
        model.addAttribute("categories", categoryService.getAllCategory());
        model.addAttribute("cartCount", GlobalData.productNewDTOList.size());
        return "about";
    }

    @GetMapping("/contact")
    public String getContact(Model model) {
        model.addAttribute("categories", categoryService.getAllCategory());
        model.addAttribute("cartCount", GlobalData.productNewDTOList.size());
        return "contact";
    }

    @GetMapping("/category/{id}")
    public String getCategory(Model model, @PathVariable Long id) {
        model.addAttribute("categories", categoryService.getAllCategory());
        model.addAttribute("category", categoryService.getCategoryById(id).get());
        model.addAttribute("products", productService.getAllProductsByCategoryId(id));
        model.addAttribute("cartCount", GlobalData.productNewDTOList.size());
        return "viewProduct";
    }

    @GetMapping("/view-product/{id}")
    public String viewProduct(Model model, @PathVariable Long id) {
        model.addAttribute("categories", categoryService.getAllCategory());
        model.addAttribute("product", productService.getProductById(id).get());

        List<ProductAttribute> productAttributeList = productAttributeService.findAllByProduct_Id(id);
        for (ProductAttribute productAttribute : productAttributeList) {
            productAttribute.getAttribute().getSize();
            productAttribute.getQuantity();
        }
        model.addAttribute("productAttributesList", productAttributeList);
        model.addAttribute("cartCount", GlobalData.productNewDTOList.size());
        return "viewSingleProduct";
    }

    @GetMapping("/view-all-products")
    public String viewAllProducts(Model model) {
        String keyword = null;
        return getAllProductsByPage(model, 1, keyword);
    }

    @GetMapping("/view-all-products/page/{pageNumber}")
    public String getAllProductsByPage(Model model, @PathVariable("pageNumber") int allProductCurrentPage, @Param("keyword") String keyword) {
        Page<Product> pageAll = productService.getAllProduct(keyword, allProductCurrentPage);
        long totalItems = pageAll.getTotalElements();
        int totalPages = pageAll.getTotalPages();

        model.addAttribute("allProductCurrentPage", allProductCurrentPage);
        model.addAttribute("totalItems", totalItems);
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("keyword", keyword);
        model.addAttribute("products", productService.getAllProduct(keyword, allProductCurrentPage));
        model.addAttribute("cartCount", GlobalData.productNewDTOList.size());
        model.addAttribute("categories", categoryService.getAllCategory());

        return "allProducts";
    }
}

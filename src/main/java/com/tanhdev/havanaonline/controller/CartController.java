package com.tanhdev.havanaonline.controller;

import com.tanhdev.havanaonline.dto.ProductDTO;
import com.tanhdev.havanaonline.dto.ProductNewDTO;
import com.tanhdev.havanaonline.global.GlobalData;
import com.tanhdev.havanaonline.model.Product;
import com.tanhdev.havanaonline.model.ProductAttribute;
import com.tanhdev.havanaonline.service.AttributeService;
import com.tanhdev.havanaonline.service.CategoryService;
import com.tanhdev.havanaonline.service.ProductAttributeService;
import com.tanhdev.havanaonline.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class CartController {

    @Autowired
    ProductService productService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    AttributeService attributeService;

    @Autowired
    ProductAttributeService productAttributeService;

    @GetMapping("/add-to-cart/{id}")
    public String addToCart(Model model, @PathVariable Long id) {
        GlobalData.productCart.add(productService.getProductById(id).get());
        return "redirect:/";
    }


    @PostMapping("/add-to-cart")
    public String addOrder(Model model, @ModelAttribute("productNewDTO") ProductNewDTO productNewDTO, @RequestParam("productId") Long productId, @RequestParam("quantity") Long quantity, @RequestParam("attributeId") Long attributeId) {
        model.addAttribute("productId", productId);
        model.addAttribute("quantity", quantity);
        model.addAttribute("attributeId", attributeId);
        //ProductNewDTO productNewDTO = new ProductNewDTO();
        Product product = productService.getProductById(productId).get();
        ProductAttribute productAttribute = productAttributeService.getProductAttributeById(productId).get();
        productNewDTO.setId(product.getId());
        productNewDTO.setName(product.getName());
        productNewDTO.setImageName(product.getImageName());
        productNewDTO.setPrice(product.getPrice());
        productNewDTO.setQuantity(quantity);
        productNewDTO.setProductAttribute(productAttribute.getAttribute().getSize());

        model.addAttribute("productNewDTO", GlobalData.productNewDTOList.add(productNewDTO));

        return "redirect:/";
    }

    @GetMapping("/cart")
    public String cartGet(Model model) {
        model.addAttribute("productNewDTO", new ProductNewDTO());
        model.addAttribute("cart", GlobalData.productNewDTOList);
        model.addAttribute("cartCount", GlobalData.productNewDTOList.size());
        //model.addAttribute("total", GlobalData.productNewDTOList.stream().mapToDouble(ProductNewDTO::getPrice).sum());
        model.addAttribute("categories", categoryService.getAllCategory());
        return "cart";
    }

    @GetMapping("/cart/remove-item/{index}")
    public String cartRemoveItem(@PathVariable int index) {
        GlobalData.productNewDTOList.remove(index);
        return "redirect:/cart";
    }
}

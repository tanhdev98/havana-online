package com.tanhdev.havanaonline.controller;

import com.tanhdev.havanaonline.dto.AttributeDTO;
import com.tanhdev.havanaonline.dto.ProductDTO;
import com.tanhdev.havanaonline.model.Attribute;
import com.tanhdev.havanaonline.model.Category;
import com.tanhdev.havanaonline.model.Product;
import com.tanhdev.havanaonline.model.ProductAttribute;
import com.tanhdev.havanaonline.service.AttributeService;
import com.tanhdev.havanaonline.service.CategoryService;
import com.tanhdev.havanaonline.service.ProductAttributeService;
import com.tanhdev.havanaonline.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Controller
public class AdminController {
    public static String uploadDir = System.getProperty("user.dir") + "/src/main/upload/productImages/";
    @Autowired
    CategoryService categoryService;

    @Autowired
    ProductService productService;

    @Autowired
    AttributeService attributeService;

    @Autowired
    ProductAttributeService productAttributeService;

    //Admin Section
    @GetMapping("/admin")
    public String adminHome() {
        return "adminHome";
    }

    //Categories Section
    @GetMapping("/admin/categories")
    public String getCategories(Model model) {
        model.addAttribute("categories", categoryService.getAllCategory());
        return "categories";
    }

    @GetMapping("/admin/categories/add")
    public String getCategoriesAdd(Model model) {
        model.addAttribute("category", new Category());
        return "categoriesAdd";
    }

    @PostMapping("/admin/categories/add")
    public String postCategoriesAdd(@ModelAttribute("category") Category category) {
        categoryService.addCategory(category);
        return "redirect:/admin/categories";
    }

    @GetMapping("/admin/categories/delete/{id}")
    public String deleteCategory(@PathVariable Long id) {
        categoryService.removeCategoryById(id);
        return "redirect:/admin/categories";
    }

    @GetMapping("/admin/categories/update/{id}")
    public String updateCategory(@PathVariable Long id, Model model) {
        Optional<Category> category = categoryService.getCategoryById(id);
        if (category.isPresent()) {
            model.addAttribute("category", category.get());
            return "categoriesAdd";
        } else return "404";
    }

    //Attributes Section
    @GetMapping("/admin/sizes")
    public String getSizes(Model model) {
        model.addAttribute("attributes", attributeService.getAllAttribute());
        return "sizes";
    }

    @GetMapping("/admin/sizes/add")
    public String getSizesAdd(Model model) {
        model.addAttribute("attribute", new Attribute());
        return "sizesAdd";
    }

    @PostMapping("/admin/sizes/add")
    public String postSizesAdd(@ModelAttribute("attributeDTO") AttributeDTO attributeDTO) {
        Attribute attribute = new Attribute();
        attribute.setId(attributeDTO.getId());
        attribute.setSize(attributeDTO.getSize());

        attributeService.addAttribute(attribute);
        return "redirect:/admin/sizes";
    }

    @GetMapping("/admin/sizes/delete/{id}")
    public String deleteSize(@PathVariable Long id) {
        attributeService.removeAttributeById(id);
        return "redirect:/admin/sizes";
    }

    @GetMapping("/admin/sizes/update/{id}")
    public String updateSize(@PathVariable Long id, Model model) {
        Attribute attribute = attributeService.getAttributeById(id).get();
        AttributeDTO attributeDTO = new AttributeDTO();

        attributeDTO.setId(attribute.getId());
        attributeDTO.setSize(attribute.getSize());

        model.addAttribute("attributeDTO", attributeDTO);
        return "sizesAdd";
    }

    @GetMapping("/admin/products/attribute/add/{id}")
    public String getAttributeAdd(Model model, @PathVariable Long id) {
        model.addAttribute("attributeDTO", new AttributeDTO());
        model.addAttribute("productAttributes", productAttributeService.getProductAttributeById(id));
        model.addAttribute("attributes", attributeService.getAllAttribute());
        model.addAttribute("products", productService.getProductById(id).get());
        return "attributeAdd";
    }

    @PostMapping("/admin/products/attribute/add/{id}")
    public String postAttributeAdd(@ModelAttribute("attributeDTO") AttributeDTO attributeDTO, @PathVariable Long id) {
        ProductAttribute productAttr = new ProductAttribute();
        Optional<Product> product = productService.getProductById(id);
        productAttr.setProduct(product.get());
        productAttr.setQuantity(attributeDTO.getQuantity());
        productAttr.setAttribute(attributeService.getAttributeById(attributeDTO.getId()).get());

        productAttributeService.addProductAttribute(productAttr);
        return "redirect:/admin/products/detail/{id}";
    }

    @GetMapping("/admin/products/attribute/delete/{id}")
    public String deleteAttribute(@PathVariable Long id) {
        productAttributeService.removeProductAttributeById(id);
        return "redirect:/admin/products/";
    }

    //Products Section
    @GetMapping("/admin/products")
    public String getProducts(Model model) {
        String keyword = null;
        return getProductsByPage(model, 1, keyword);
    }

    @GetMapping("/admin/products/page/{pageNumber}")
    public String getProductsByPage(Model model, @PathVariable("pageNumber") int currentPage, @Param("keyword") String keyword) {
        Page<Product> page = productService.getAllProduct(keyword, currentPage);
        long totalItems = page.getTotalElements();
        int totalPages = page.getTotalPages();
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("totalItems", totalItems);
        model.addAttribute("totalPages", totalPages);
        model.addAttribute("keyword", keyword);
        model.addAttribute("products", productService.getAllProduct(keyword, currentPage));
        return "products";
    }

    @GetMapping("/admin/products/add")
    public String getProductsAdd(Model model) {
        model.addAttribute("productDTO", new ProductDTO());
        model.addAttribute("categories", categoryService.getAllCategory());
        return "productsAdd";
    }

    @PostMapping("admin/products/add")
    public String postProductsAdd(@ModelAttribute("productDTO") ProductDTO productDTO, @RequestParam("productImage") MultipartFile file, @RequestParam("imgName") String imgName) throws IOException {
        Product product = new Product();
        product.setId(productDTO.getId());
        product.setName(productDTO.getName());
        product.setCategory(categoryService.getCategoryById(productDTO.getCategoryId()).get());
        product.setPrice(productDTO.getPrice());
        product.setTag(productDTO.getTag());
        product.setDescription(productDTO.getDescription());

        //add product image
        String imageUUID;
        if (!file.isEmpty()) {
            imageUUID = file.getOriginalFilename();
            Path fileNameAndPath = Paths.get(uploadDir, imageUUID);
            Files.write(fileNameAndPath, file.getBytes());
        } else {
            imageUUID = imgName;
        }
        product.setImageName(imageUUID);
        productService.addProduct(product);

        return "redirect:/admin/products";
    }

    @GetMapping("/admin/products/delete/{id}")
    public String deleteProduct(@PathVariable Long id) {
        productService.removeProductById(id);
        return "redirect:/admin/products";
    }

    @GetMapping("/admin/products/detail/{id}")
    public String detailProduct(Model model, @PathVariable Long id) {
        model.addAttribute("products", productService.getProductById(id).get());
        List<ProductAttribute> productAttributeList = productAttributeService.findAllByProduct_Id(id);
        for (ProductAttribute productAttribute : productAttributeList) {
            productAttribute.getAttribute().getSize();
            productAttribute.getQuantity();
        }
        model.addAttribute("productAttributesList", productAttributeList);
        return "productDetail";
    }

    @GetMapping("/admin/products/update/{id}")
    public String updateProductGet(@PathVariable Long id, Model model) {
        Product product = productService.getProductById(id).get();

        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(product.getId());
        productDTO.setName(product.getName());
        productDTO.setCategoryId(product.getCategory().getId());
        productDTO.setPrice(product.getPrice());
        productDTO.setTag(product.getTag());
        productDTO.setDescription(product.getDescription());
        productDTO.setImageName(product.getImageName());

        model.addAttribute("categories", categoryService.getAllCategory());
        model.addAttribute("productDTO", productDTO);

        return "productsAdd";
    }
}

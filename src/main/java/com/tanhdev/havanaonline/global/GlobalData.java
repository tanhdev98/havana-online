package com.tanhdev.havanaonline.global;

import com.tanhdev.havanaonline.dto.ProductNewDTO;
import com.tanhdev.havanaonline.model.Product;

import java.util.ArrayList;
import java.util.List;

public class GlobalData {
    public static List<Product> productCart;

    public static List<ProductNewDTO> productNewDTOList;

    static {
        productCart = new ArrayList<Product>();
    }

    static {
        productNewDTOList = new ArrayList<ProductNewDTO>();
    }
}
